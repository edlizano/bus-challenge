package com.goeuro.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoeuroChallengeEdisonApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoeuroChallengeEdisonApplication.class, args);
	}
}
