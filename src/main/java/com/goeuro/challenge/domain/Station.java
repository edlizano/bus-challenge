package com.goeuro.challenge.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by forest on 4/12/16.
 */
public class Station {

    private int id;

    private Set<Integer> routes;

    public Station() {
    }

    public Station(int id) {

        this.id = id;
        this.routes = new HashSet<Integer>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Integer> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<Integer> routes) {
        this.routes = routes;
    }
}
