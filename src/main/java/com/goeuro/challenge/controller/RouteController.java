package com.goeuro.challenge.controller;

import com.goeuro.challenge.dto.RouteResponseDto;
import com.goeuro.challenge.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by forest on 4/12/16.
 */

@RestController
@RequestMapping("/api")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @RequestMapping("/direct")
    public RouteResponseDto findRoute(@RequestParam(value = "arr_sid") int arrivalStationId,
                                      @RequestParam(value = "dep_sid") int departureStationId){

        return routeService.hasDirectRoute(arrivalStationId, departureStationId);
    }
}
