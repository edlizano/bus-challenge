package com.goeuro.challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by forest on 4/12/16.
 */
public class RouteResponseDto {

    @JsonProperty(value = "arr_sid")
    private int arrivalStationId;

    @JsonProperty(value = "dep_sid")
    private int departureStationId;

    @JsonProperty(value = "direct_bus_route")
    boolean direct;

    public RouteResponseDto() {
    }

    public RouteResponseDto(int arrivalStationId, int departureStationId, boolean direct) {
        this.arrivalStationId = arrivalStationId;
        this.departureStationId = departureStationId;
        this.direct = direct;
    }

    public int getArrivalStationId() {
        return arrivalStationId;
    }

    public void setArrivalStationId(int arrivalStationId) {
        this.arrivalStationId = arrivalStationId;
    }

    public int getDepartureStationId() {
        return departureStationId;
    }

    public void setDepartureStationId(int departureStationId) {
        this.departureStationId = departureStationId;
    }

    public boolean isDirect() {
        return direct;
    }

    public void setDirect(boolean direct) {
        this.direct = direct;
    }

}
