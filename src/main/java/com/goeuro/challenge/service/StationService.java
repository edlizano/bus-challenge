package com.goeuro.challenge.service;

import com.goeuro.challenge.domain.Station;
import org.springframework.boot.CommandLineRunner;

import java.io.IOException;
import java.util.Map;

/**
 * Created by forest on 4/12/16.
 */
public interface StationService extends CommandLineRunner{

    void loadRoutes(String filePath) throws IOException, NumberFormatException;

    Station getStationById(int id);

    Map<Integer, Station> getStationCache();
}
