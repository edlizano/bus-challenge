package com.goeuro.challenge.service;

import com.goeuro.challenge.domain.Station;
import com.goeuro.challenge.dto.RouteResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by forest on 4/12/16.
 */
@Service
public class RouteServiceImp implements RouteService{

    @Autowired
    StationService stationService;


    @Override
    public RouteResponseDto hasDirectRoute(int departureStationId, int arrivalStationId) {

        Station departureStation = stationService.getStationCache().get(departureStationId);
        Station arrivalStation = stationService.getStationCache().get(arrivalStationId);
        boolean isDirect = false;

        if(departureStation != null && arrivalStation != null) {

            Set<Integer> intersection = new HashSet<>(departureStation.getRoutes());

            intersection.retainAll(arrivalStation.getRoutes());

            isDirect = !intersection.isEmpty();

        }else{
            System.out.println("Could not find station in cache...");
        }

        return new RouteResponseDto(departureStationId, arrivalStationId, isDirect);
    }

}
