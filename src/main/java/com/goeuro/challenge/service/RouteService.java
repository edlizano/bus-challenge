package com.goeuro.challenge.service;

import com.goeuro.challenge.dto.RouteResponseDto;

/**
 * Created by forest on 4/12/16.
 */
public interface RouteService {

    RouteResponseDto hasDirectRoute(int departureStationId, int arrivalStationId);

}
