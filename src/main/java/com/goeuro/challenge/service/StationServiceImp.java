package com.goeuro.challenge.service;

import com.goeuro.challenge.domain.Station;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Created by forest on 4/12/16.
 */
@Service
public class StationServiceImp implements StationService {

    private Map<Integer, Station> stationCache;

    private static final String DEFAULT_ROUTE_FILE_PATH = "./routes.txt";
    private static final int MIN_ROUTE_DATA_LENGTH = 3;
    private static final int ROUTE_ID_INDEX = 0;

    @Override
    public void run(String... strings) throws Exception {

        String path = getFilePath(strings);

        try{
            loadRoutes(path);

        }catch (NumberFormatException | IOException e) {
            System.out.println("ERROR: Invalid data format in Route file");
            System.out.println(e.getMessage());
        }
    }

    private String getFilePath(String[] strings) {

        String path;

        if(strings.length > 0) {
            System.out.println("Loading routes file from custom path: " + strings[0]);
            path = strings[0];
        }else{
            System.out.println("Loading routes file from default path: " + DEFAULT_ROUTE_FILE_PATH);
            path = DEFAULT_ROUTE_FILE_PATH;
        }
        return path;
    }

    @Override
    public void loadRoutes(String filePath) throws IOException, NumberFormatException {

        System.out.println("Loading routes...");
        long start = System.currentTimeMillis();

        BufferedReader br = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8);

        int numRoutes = Integer.valueOf(br.readLine());
        stationCache = new HashMap<>(numRoutes);

        String line;
        while ((line = br.readLine()) != null) {
            processLine(line);
        }

        long elapsedTime = System.currentTimeMillis() - start;

        System.out.println("Routes successfully loaded... process took: " + elapsedTime + "ms");

    }

    public void processLine (String line) throws NumberFormatException{

        String [] data = line.split(" ");

        if(data.length >= MIN_ROUTE_DATA_LENGTH){

            int routeId = Integer.valueOf(data[ROUTE_ID_INDEX]);

            IntStream.range(ROUTE_ID_INDEX + 1, data.length).forEachOrdered(i -> {

                Station station = getStationById(Integer.valueOf(data[i]));
                station.getRoutes().add(routeId);

            });

        }else{
            System.out.println("Invalid Route Record...");
        }

    }

    @Override
    public Station getStationById(int id) {

        if(stationCache.containsKey(id)) {
            return stationCache.get(id);
        }else {
            Station station = new Station(id);
            stationCache.put(id, station);
            return station;
        }
    }


    @Override
    public Map<Integer, Station> getStationCache() {
        return stationCache;
    }

}
